//
//  Exante_MarketsTests.swift
//  Exante-MarketsTests
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import XCTest
import Exante_Markets

class Exante_MarketsTests: XCTestCase {

    // https://developers.exante.eu/tutorials/auth-basics/
    
    func testJSONBase64Encode() {
        let testStr = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        let resultStr = EMCrypt.encodeJSON(["typ": "JWT", "alg": "HS256"])
        
        XCTAssert(testStr == resultStr, "JSON-Base64")
    }
    
}
