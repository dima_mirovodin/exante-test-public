//
//  EMSymbolsModel.swift
//  Exante-Markets
//
//  Created by Dmitri Mirovodin on 4/18/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMSymbolsModel: NSObject {
  
    private let storageId: String
    
    private static let defaultSymbols = ["MSFT.NASDAQ", "AAPL.NASDAQ", "GOOGL.NASDAQ", "INTC.NASDAQ", "YHOO.NASDAQ", "GAZP.MICEX"]
    public var symbols = [String]()
    
    public init(storageId: String) {
        self.storageId = storageId
        super.init()
    }
    
    public func load() {
        let defaults = UserDefaults.standard
        symbols = defaults.stringArray(forKey: storageId) ?? EMSymbolsModel.defaultSymbols
    }
    
    public func save() {
        let defaults = UserDefaults.standard
        defaults.set(symbols, forKey: storageId)
    }
}
