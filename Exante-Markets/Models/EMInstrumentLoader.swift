//
//  EMInstrumentLoader.swift
//  Exante-Markets
//
//  Created by Dmitri Mirovodin on 4/20/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

protocol EMInstrumentLoader {
    func loadInstrument() -> [EMInstrument]
}

class EMFileInstrumentLoader: NSObject, EMInstrumentLoader {
    
    private var parser: EMInstrumentParser
    
    public init(parser: EMInstrumentParser) {
        self.parser = parser
        super.init()
    }
    
    public func loadInstrument() -> [EMInstrument] {
        
        if let filepath = Bundle.main.path(forResource: "symbols", ofType: "json") {
            do {
                let url = URL(fileURLWithPath: filepath)
                let data = try Data(contentsOf: url)
                return parser.parser(data: data)
            } catch {
                NSLog("> Error: Unable to load instruments.")
            }
        }
        return []
    }
}

class EMNetInstrumentLoader: NSObject, EMInstrumentLoader {
    
    private var parser: EMInstrumentParser
    
    public init(parser: EMInstrumentParser) {
        self.parser = parser
        super.init()
    }
    
    public func loadInstrument() -> [EMInstrument] {
        guard let url = EMApiEndPoint.shared.symbolsURL()  else {
            return []
        }
        let session = URLSession.shared
        let semaphore = DispatchSemaphore(value: 0)
        var data: Data?
        let task = session.dataTask(with: url) { (dataReceived: Data?, respose:URLResponse?, error:Error?) in
            data = dataReceived
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
        
        if let data = data {
            return parser.parser(data: data)
        }
        
        return []
    }
}
