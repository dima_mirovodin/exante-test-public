//
//  EMQuotesModel.swift
//  Exante-Markets
//
//  Created by Dmitri Mirovodin on 4/18/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMQuotesModel: NSObject {
    
    public var onQuotesChanged: ((Set<String>) -> Void)?
    public var onHeartbeat: (() -> Void)?
    
    private var quotes = [String:EMQuote]()
    private var subscription: EMQuotesSubscription
    
    public var isNotificationPaused: Bool = false
    
    override public init() {
        subscription = EMQuotesNetSubscription(parser: EMDefaultQuotesParser())
        
        super.init()
        
        self.subscription.onQuotesChanged = { [weak self] (quotes: [EMQuote]) in
            if let sSelf = self, let block = sSelf.onQuotesChanged, !sSelf.isNotificationPaused {
                sSelf.updateQuotes(quotes)
                
                let set = Set(quotes.map { $0.symbol })
                
                DispatchQueue.main.async {
                    block(set)
                }
            }
        }
        
        self.subscription.onHeartbeat = { [weak self] in
            if let sSelf = self, let block = sSelf.onHeartbeat, !sSelf.isNotificationPaused {
                DispatchQueue.main.async {
                    block()
                }
            }
        }        
    }
    
    deinit {
        subscription.unSubscribe()
    }
    
    public func getQuote(symbol: String) -> EMQuote? {
        return quotes[symbol]
    }
    
    public func subscribe(symbols:Set<String>) {
        subscription.subscribe(symbols: symbols)
    }
    
    public func unSubscribe() {
        subscription.unSubscribe()
    }
    
    private func updateQuotes(_ newQuotes: [EMQuote]) {
        
        for newQuote in newQuotes {
            if var quote = quotes[newQuote.symbol] {
                quote.askDirection = EMUtils.calcQuoteDirection(oldValue: quote.ask, newValue: newQuote.ask)
                quote.bidDirection = EMUtils.calcQuoteDirection(oldValue: quote.bid, newValue: newQuote.bid)
                quote.bid = newQuote.bid
                quote.ask = newQuote.ask                
                quotes[newQuote.symbol] = quote
            }
            else {
                quotes[newQuote.symbol] = newQuote
            }
        }
    }
    
}
