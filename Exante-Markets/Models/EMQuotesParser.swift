//
//  EMQuotesParser.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

protocol EMQuotesParser {
    var isHeartbeat: Bool { get }
    var quotes: [EMQuote] { get }
    func parser(data: Data) -> Bool
}

class EMDefaultQuotesParser: NSObject,  EMQuotesParser {
    
    private static let heartbeat = ":heartbeat"
    private static let dataPrefix = "data: "
    private static let minDataLineLength = 20
    
    public var isHeartbeat: Bool = false
    public var quotes: [EMQuote] = []
    
    func parser(data: Data) -> Bool {
        quotes = []
        isHeartbeat = false
        
        guard let str = String(data: data, encoding: .utf8) else {
            return false
        }
        
        if str.contains(EMDefaultQuotesParser.heartbeat) {
            isHeartbeat = true
            return true
        }
        else {
            let lines = str.components(separatedBy: "\n")
            
            for line in lines {
                
                guard (line.characters.count > EMDefaultQuotesParser.minDataLineLength) else {
                    continue
                }
                guard line.hasPrefix(EMDefaultQuotesParser.dataPrefix) else {
                    continue
                }
                
                let chars = line.characters.dropFirst(EMDefaultQuotesParser.dataPrefix.characters.count)
                
                if let data = String(chars).data(using: .utf8) {
                    
                    do {
                        if let item = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
                            if let symbol = item["symbolId"] as? String, let ask = item["ask"] as? NSNumber, let bid = item["bid"] as? NSNumber {
                                let quote = EMQuote(symbol: symbol, bid: Double(bid), ask: Double(ask))
                                quotes.append(quote)
                            }
                        }
                    } catch {
                        NSLog("> Error: Unable to parse line. Line is \(line) ")
                    }
                }
            }
        }
        return quotes.count > 0
    }
}
