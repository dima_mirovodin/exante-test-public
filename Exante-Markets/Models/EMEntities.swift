//
//  EMEntities.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

enum EMQuoteDirection {
    case up, down, none
}

struct EMQuote {
    var symbol:String
    var bid:Double
    var bidDirection: EMQuoteDirection
    var ask:Double
    var askDirection: EMQuoteDirection
        
    init() {
        self.symbol = ""
        self.bid = 0.0
        self.ask = 0.0
        self.bidDirection = .none
        self.askDirection = .none
    }
    
    init(symbol: String, bid:Double, ask:Double) {
        self.symbol = symbol
        self.bid = bid
        self.ask = ask
        self.bidDirection = .none
        self.askDirection = .none
    }
}

struct EMInstrument {
    let symbol:String
    let name: String
    let description: String
    let group: EMInstrumentGroup
    
    init(symbol: String, groupName:String, name:String, description: String) {
        self.symbol = symbol
        self.group = EMInstrumentGroups.sharedInstance.getGroup(groupName: groupName)
        self.name = name
        self.description = description
    }
}

