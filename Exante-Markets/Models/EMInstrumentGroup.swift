//
//  EMInstrumentGroups.swift
//  Exante-Markets
//
//  Created by Dmitri Mirovodin on 4/20/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMInstrumentGroup: NSObject {

    public static let EMPTY = EMInstrumentGroup(groupName: "", groupId: -1)
    
    public let groupName: String
    public let groupId: Int
    
    fileprivate init(groupName: String, groupId: Int) {
        self.groupName = groupName
        self.groupId = groupId
        super.init()
    }
    
    override var hash: Int {
        return groupName.hash
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let other = object as? EMInstrumentGroup {
            return (groupId == other.groupId)
        } else {
            return false
        }
    }
}

class EMInstrumentGroups: NSObject {
    private var groups = [String: EMInstrumentGroup]()
    private var counter = 0
    static let sharedInstance = EMInstrumentGroups()
    
    private override init() {
        super.init()
    }
    
    public func count () -> Int {
        return counter
    }
    
    public func getGroup(groupName: String) -> EMInstrumentGroup {
        if let group = groups[groupName] {
            return group
        }
        
        let group = EMInstrumentGroup(groupName: groupName, groupId: counter)
        groups[groupName] = group
        
        counter += 1
        
        return group
    }
}
