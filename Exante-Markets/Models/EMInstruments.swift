//
//  EMInstruments.swift
//  Exante-Markets
//
//  Created by Dmitri Mirovodin on 4/19/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit


struct EMInstruments {
    var group: EMInstrumentGroup = EMInstrumentGroup.EMPTY
    var items = [EMInstrument]()
}

class EMInstrumentStorage: NSObject {
    
    private var loader: EMInstrumentLoader
    private var allInstruments = [EMInstruments]()
    private var filteredInstruments = [EMInstruments]()
    private var isFiltered = false

    public var instruments: [EMInstruments] {
        get {
            let result = isFiltered ? filteredInstruments : allInstruments
            return result
        }
    }
    
    public init(instrumentLoader: EMInstrumentLoader) {
        self.loader = instrumentLoader
        super.init()
    }
    
    // MARK: - Public methods
    
    public func loadInstruments(completion: @escaping ([EMInstruments]) -> Void) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            if let sSelf = self {
                NSLog("> Request instruments.")
                let loadedList = sSelf.loader.loadInstrument()
                sSelf.allInstruments = sSelf.groupInstruments(list: loadedList)
            }
            
            DispatchQueue.main.async { [weak self] in
                if let sSelf = self {
                    NSLog("> Instruments were loaded.")
                    completion(sSelf.allInstruments)
                }
            }
        }
    }

    public func filterFor(text: String, completion: @escaping ([EMInstruments]) -> Void) {
        NSLog("> Instrument searching - start.")
        let letterCount = text.characters.count
        if (letterCount <= 1) {
            if (letterCount == 0) {
                isFiltered = false
            }
            NSLog("> Instrument searching - finished.")
            let result = allInstruments
            completion(result)
        }
        else {
            DispatchQueue.global(qos: .background).async { [weak self] in
                if let sSelf = self {
                    if (text.characters.count == 0) {
                        sSelf.isFiltered = false
                    }
                    else {
                        let groupCount = EMInstrumentGroups.sharedInstance.count()
                        var result = [EMInstruments](repeating: EMInstruments(), count: groupCount)
                        
                        for instrumentGroup in sSelf.allInstruments {
                            for instrument in instrumentGroup.items {
                                if (sSelf.passedFilter(query: text, instrument: instrument)) {
                                    result[instrument.group.groupId].group = instrument.group
                                    result[instrument.group.groupId].items.append(instrument)
                                }
                            }
                        }
                        
                        sSelf.filteredInstruments = result.filter({ (instruments: EMInstruments) -> Bool in
                            return instruments.items.count > 0
                        })
                        
                        sSelf.isFiltered = true
                    }
                }
                
                DispatchQueue.main.async { [weak self] in
                    if let sSelf = self {
                        NSLog("> Instrument searching - finished.")
                        completion(sSelf.instruments)
                    }
                }
            }
        }
    }
    
    // MARK: - Private methods 
    
    private func passedFilter(query: String, instrument: EMInstrument) -> Bool {
        
        return instrument.description.localizedCaseInsensitiveContains(query)
    }
    
    private func groupInstruments(list:[EMInstrument]) -> [EMInstruments] {
        let groupCount = EMInstrumentGroups.sharedInstance.count()
        var result = [EMInstruments](repeating: EMInstruments(), count: groupCount)
        
        for instrument in list {
            result[instrument.group.groupId].group = instrument.group
            result[instrument.group.groupId].items.append(instrument)
        }
        
        result.sort {
            $0.group.groupName < $1.group.groupName
        }
        
        return result
    }
    
}
