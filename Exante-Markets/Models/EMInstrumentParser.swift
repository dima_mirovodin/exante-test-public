//
//  EMInstrumentParser.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

protocol EMInstrumentParser {
    func parser(data: Data) -> [EMInstrument]
}

class EMJsonInstrumentParser: NSObject, EMInstrumentParser {
    
    func parser(data: Data) -> [EMInstrument] {
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                var result = [EMInstrument]()
                result.reserveCapacity(json.count)
                for item in json  {
                    if let item = item as? NSDictionary {
                        if let symbol = item["id"] as? String, let name = item["name"] as? String, let groupName = item["type"] as? String, let description = item["description"] as? String {
                            let instrument = EMInstrument(symbol: symbol, groupName: groupName, name: name, description: description)
                            result.append(instrument)
                        }
                    }
                }
                return result
            }
        } catch {
            NSLog("> Error: Unable to convert instruments.")
        }
        return []
    }
}
