//
//  EMQuotesSubscription.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

protocol EMQuotesSubscription {
    var onQuotesChanged: (([EMQuote]) -> Void)? { get set }
    var onHeartbeat: (() -> Void)? { get set }
    func subscribe(symbols: Set<String>)
    func unSubscribe()    
}
