//
//  EMQuotesNetSubscription.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit


class EMQuotesNetSubscription: NSObject, EMQuotesSubscription, URLSessionDelegate, URLSessionDataDelegate, URLSessionTaskDelegate {
    
    public var onQuotesChanged: (([EMQuote]) -> Void)?
    public var onHeartbeat: (() -> Void)?
    
    private var session: URLSession?
    private var sessionTask: URLSessionTask?
    private var quoteParser : EMQuotesParser
    
    public init(parser: EMQuotesParser) {
        quoteParser = parser
        super.init()
        let configuration = URLSessionConfiguration.default
        
        session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }
    
    deinit {
        unSubscribe()
        if let session = session {
            session.invalidateAndCancel()
        }
    }
    
    // MARK: - NSURLSessionDelegate
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        completionHandler(URLSession.AuthChallengeDisposition.performDefaultHandling, credential);
    }
    
    // MARK: - URLSessionDataDelegate
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {

        if quoteParser.parser(data: data) {
            if let block = onHeartbeat, quoteParser.isHeartbeat {
                block()
            }
        
            if let block = onQuotesChanged, quoteParser.quotes.count > 0 {
                block(quoteParser.quotes)
            }
        }
    }
    
    // MARK: - EMQuotesSubscription
    
    func subscribe(symbols: Set<String>) {
        if let url = EMApiEndPoint.shared.quotesURL(symbols: symbols), let session = session {
            unSubscribe()
            NSLog("> Subscribe to symbols.")
            sessionTask = session.dataTask(with: url)
            sessionTask?.resume()
        }
    }
    
    func unSubscribe() {
        NSLog("> Unsubscribe from symbols.")
        if let task = sessionTask {
            if task.state == .running {
                task.cancel()
            }
        }
    }
    
}
