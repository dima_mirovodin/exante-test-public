//
//  EMUtils.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMUtils: NSObject {
    public static func formatPrice(_ price: Double) -> String {
        return String(format:"%.3f", price)
    }
    
    public static func calcQuoteDirection(oldValue: Double, newValue: Double) -> EMQuoteDirection {
        if (oldValue > newValue) {
            return .down
        } else if (oldValue < newValue) {
            return .up
        }
        return .none
    }
}
