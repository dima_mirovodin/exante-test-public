//
//  EMCrypt.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

public class EMCrypt: NSObject {
    
    public static func base64encode(_ input: Data) -> String {
        let data = input.base64EncodedData(options: NSData.Base64EncodingOptions(rawValue: 0))
        let string = String(data: data, encoding: .utf8)!
        return string
            .replacingOccurrences(of: "+", with: "-", options: NSString.CompareOptions(rawValue: 0), range: nil)
            .replacingOccurrences(of: "/", with: "_", options: NSString.CompareOptions(rawValue: 0), range: nil)
            .replacingOccurrences(of: "=", with: "", options: NSString.CompareOptions(rawValue: 0), range: nil)
    }
    
    public static func encodeJSON(_ input: [String: Any]) -> String {
        if let data = try? JSONSerialization.data(withJSONObject: input) {
            return EMCrypt.base64encode(data)
        }
        return ""
    }

    public static func signSHA256(inputString: String, key: String) -> String {
        let cKey = key.cString(using: String.Encoding.utf8)
        let cData = inputString.cString(using: String.Encoding.utf8)
        var result = [CUnsignedChar](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), cKey!, Int(strlen(cKey!)), cData!, Int(strlen(cData!)), &result)
        let data = NSData(bytes: result, length: (Int(CC_SHA256_DIGEST_LENGTH)))
        return EMCrypt.base64encode(data as Data)
    }
}
