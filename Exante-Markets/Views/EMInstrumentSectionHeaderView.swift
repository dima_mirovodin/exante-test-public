//
//  EMInstrumentSectionHeaderView.swift
//  Exante-Markets
//
//  Created by Dmitri Mirovodin on 4/19/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMInstrumentSectionHeaderView: EMTableViewHeaderFooterView {

    @IBOutlet weak var headerLabel: UILabel!
}
