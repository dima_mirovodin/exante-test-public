//
//  EMQuoteCellView.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMQuoteCellView: UITableViewCell {

    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var bidLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [symbolLabel, askLabel, bidLabel].forEach { (label:UILabel) in
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.7
        }
    }

    public func updateQuote(quote: EMQuote?) {
        if let quote = quote {
            askLabel.text = EMUtils.formatPrice(quote.ask)
            bidLabel.text = EMUtils.formatPrice(quote.bid)
            askLabel.textColor = getColor(quote.askDirection)
            bidLabel.textColor = getColor(quote.bidDirection)
        } else {
            askLabel.text = "-"
            bidLabel.text = "-"
            askLabel.textColor = getColor(.none)
            bidLabel.textColor = getColor(.none)
        }
    }
    
    private func getColor(_ direction: EMQuoteDirection) -> UIColor {
        switch direction {
            case .down:
                return UIColor.red
            case .up :
                return UIColor.green
            default:
                return UIColor.black
        }
    }
}
