//
//  EMTableViewHeaderFooterView.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMTableViewHeaderFooterView: UITableViewHeaderFooterView {

    override public func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.white
        backgroundView = UIView()
        backgroundView?.backgroundColor = UIColor.white
    }

}
