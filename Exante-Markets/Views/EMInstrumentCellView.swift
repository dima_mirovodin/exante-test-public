//
//  EMInstrumentCellView.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMInstrumentCellView: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    public var query:String = "" {
        didSet {
            if let text = descriptionLabel.text, query.characters.count > 1, let range = text.range(of: query, options: .caseInsensitive)  {
                let start = text.distance(from: text.startIndex, to: range.lowerBound)
                let end = text.distance(from: text.startIndex, to: range.upperBound)
                let range = NSRange(location: start, length: end - start)
                let html = NSMutableAttributedString(string: text)
                html.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: range)
                descriptionLabel.attributedText = html
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.minimumScaleFactor = 0.7
        descriptionLabel.numberOfLines = 2
        descriptionLabel.lineBreakMode = .byTruncatingTail
    }
}
