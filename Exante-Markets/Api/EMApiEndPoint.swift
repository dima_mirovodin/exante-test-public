//
//  EMApiEndPoint.swift
//  Exante-Markets
//
//  Created by Dmitry Mirovodin on 06/05/2017.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMApiEndPoint: NSObject {
    
    // https://developers.exante.eu/api/
    // https://jwt.io

    private static let iss = "076c1a5a-bcc0-41e5-8ee6-172c26d73c17"     // ClientId
    private static let sub = "02b1c079-8499-4a63-84dd-46d1254373d8"     // AppId
    private static let key = "kgqlgb0YtgPWxesONocStQOfIhZPNMvJ"         // Shared key
    private static let base = "https://api-demo.exante.eu/md/1.0/"
    
    private var token = ""
    private var tokenExpire: Int = 0
    
    public static let shared = EMApiEndPoint()
    
    override private init() {
        super.init()
    }
    
    // MARK: - Public Methods
    
    public func symbolsURL() -> URL? {
        let s = EMApiEndPoint.base + "symbols?token=" + getToken()        
        return URL(string: s)
    }
    
    public func quotesURL(symbols: Set<String>) -> URL? {
        let list = symbols.joined(separator: ",")
        let s = EMApiEndPoint.base + "feed/" + list + "?token=" + getToken()
        return URL(string: s)
    }

    // MARK: - Token
    
    private func getToken() -> String {
        let currentTimeStamp = Int(Date().addingTimeInterval(TimeInterval(5 * 60)).timeIntervalSince1970)
        if (token.characters.count == 0 || tokenExpire <= currentTimeStamp) {
            token = getSignature()
        }
        return token
    }
    
    private func getPayload(iss: String, sub: String, iat: Int, exp: Int) -> String {
        let payload = EMCrypt.encodeJSON(["iss": iss,
                                          "sub": sub,
                                          "iat": iat,
                                          "exp": exp,
                                          "aud": ["symbols","ohlc","feed","change","crossrates"]])
        return payload
    }
    
    private func getSignature() -> String {
        let iat = Int(Date().timeIntervalSince1970)
        let exp = Int(Date().addingTimeInterval(TimeInterval(60 * 60)).timeIntervalSince1970)
        tokenExpire = exp
        let header = EMCrypt.encodeJSON(["typ": "JWT", "alg": "HS256"])
        let payload = getPayload(iss: EMApiEndPoint.iss, sub: EMApiEndPoint.sub, iat: iat, exp: exp)
                
        let signingInput = "\(header).\(payload)"
        let signature = EMCrypt.signSHA256(inputString: signingInput, key: EMApiEndPoint.key)
        
        return "\(signingInput).\(signature)"
    }
}
