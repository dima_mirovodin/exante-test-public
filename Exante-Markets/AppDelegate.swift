//
//  AppDelegate.swift
//  Exante-Martes
//
//  Created by Dmitri Mirovodin on 4/18/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let marketController = EMQuoteListViewController()
        let rootController = UINavigationController()
        rootController.navigationBar.isTranslucent = false
        rootController.setViewControllers([marketController], animated: false)
        
        window =  UIWindow(frame: UIScreen.main.bounds)
        if let window = self.window {
            window.rootViewController = rootController
            window.makeKeyAndVisible()
        }
        
        return true
    }
}

