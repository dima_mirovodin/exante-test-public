//
//  EMMarketListViewController.swift
//  Exante-Markets
//
//  Created by Dmitri Mirovodin on 4/18/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit

class EMQuoteListViewController: UITableViewController {
    
    static private let headerId = "QuoteListHeaderId"
    static private let cellId = "QuoteListCellId"
    static private let holderId = "QuoteList"
    
    private let symbolsModel = EMSymbolsModel(storageId: EMQuoteListViewController.holderId)
    private let quotes = EMQuotesModel()
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Quotes"
     
        let button = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(onEditButtonTap))
        navigationItem.rightBarButtonItem = button
        
        tableView.register(UINib(nibName: "EMQuoteListHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: EMQuoteListViewController.headerId)
        tableView.register(UINib(nibName: "EMQuoteCellView", bundle: nil), forCellReuseIdentifier: EMQuoteListViewController.cellId)
        tableView.sectionHeaderHeight = 40
        tableView.rowHeight = 35
        tableView.separatorStyle = .none
        
        quotes.onQuotesChanged = { [weak self] (symbols: Set<String>) in
            if let sSelf = self {
                sSelf.updateTableByQuotes()
                let d = Date()
                NSLog("> Quotes were changed: \(d)")
                
            }
        }
        quotes.onHeartbeat = { 
            let d = Date()
            NSLog("> Heartbeat: \(d)")
        }
        
        symbolsModel.load()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
        updateSubscription()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        quotes.unSubscribe()
    }
    
    // MARK: - Quotes 
    
    private func updateTableByQuotes() {
       
        if let visibleRows = tableView.indexPathsForVisibleRows {
            tableView.beginUpdates()
            tableView.reloadRows(at: visibleRows, with: .none)
            tableView.endUpdates()
        }
    }
    
    private func updateSubscription() {
        let set = Set(symbolsModel.symbols.map { $0 })
        quotes.subscribe(symbols: set)
    }
    
    // MARK: - Actions
    
    @objc private func onEditButtonTap() {
        let controller = EMInstrumentListViewController()
        controller.onAddSymbolsCompletion = { [weak self] (symbols: Set<String>) -> Void in
            if let sSelf = self {
                for item in symbols {                    
                    if (!sSelf.symbolsModel.symbols.contains(item)) {
                        sSelf.symbolsModel.symbols.append(item)
                    }
                }
                sSelf.saveSymbols()
            }
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - UITableViewDataSource
     
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return symbolsModel.symbols.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EMQuoteListViewController.cellId) as! EMQuoteCellView
        
        let symbol = symbolsModel.symbols[indexPath.row]
        cell.symbolLabel.text = symbol
        let quote = quotes.getQuote(symbol: symbol)
        cell.updateQuote(quote: quote)
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            symbolsModel.symbols.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            saveSymbols()
        }
    }
    
    override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        quotes.isNotificationPaused = true
    }
    
    override func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        quotes.isNotificationPaused = false
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: EMQuoteListViewController.headerId)
    }
    
    // MARK: - Private methods
    
    private func saveSymbols () {
        symbolsModel.save()
    }
}
