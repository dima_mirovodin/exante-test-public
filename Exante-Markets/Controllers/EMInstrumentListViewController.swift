//
//  EMInstrumentListViewController.swift
//  Exante-Markets
//
//  Created by Dmitri Mirovodin on 4/18/17.
//  Copyright © 2017 Dmitry Mirovodin. All rights reserved.
//

import UIKit


class EMInstrumentListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    enum Status {
        case loading, searching, error, empty
    }
    
    private static let cellId = "EMInstrumentListViewControllerCellId"
    private static let sectionId = "EMInstrumentListViewControllerSectionId"
    
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var informationIndicator: UIActivityIndicatorView!
    @IBOutlet weak var informationTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!

    private var storage: EMInstrumentStorage = EMInstrumentStorage(instrumentLoader: EMNetInstrumentLoader(parser: EMJsonInstrumentParser()))
    private var instruments = [EMInstruments]()
    private var addButton: UIBarButtonItem?
    private var selectedSymbols = Set<String>()

    public var onAddSymbolsCompletion: ((Set<String>) -> Void)?

    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Instruments"
        
        initializeTableView()
        initializeView()
        requestInstruments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Actions
    
    @objc private func onViewTap() {
        view.endEditing(true)
    }
    
    @objc private func onAddButtonTap() {
        if let block = onAddSymbolsCompletion {
            block(selectedSymbols)
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc private func keyboardDidShow(_ notification:NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tableBottomConstraint.constant = keyboardHeight + 8.0
        }
    }
    
    @objc private func keyboardWillHide(_ notification:NSNotification) {
        tableBottomConstraint.constant = 0
    }
    
    // MARK: - Private methods
    
    private func initializeTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.sectionHeaderHeight = 40
        tableView.rowHeight = 50
        tableView.separatorStyle = .none
        tableView.allowsSelection = true
        
        tableView.register(UINib(nibName: "EMInstrumentCellView", bundle: nil), forCellReuseIdentifier: EMInstrumentListViewController.cellId)
        tableView.register(UINib(nibName: "EMInstrumentSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: EMInstrumentListViewController.sectionId)
    }
    
    private func initializeView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onViewTap))
        tapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecognizer)
        
        addButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(onAddButtonTap))
        navigationItem.rightBarButtonItem = addButton
        addButton?.isEnabled = false
        
        searchTextField.delegate = self
        
        informationViewHide()
    }
    
    private func requestInstruments() {
        informationViewShow(.loading)
        
        storage.loadInstruments { [weak self] (items:[EMInstruments]) in
            if let sSelf = self {
                sSelf.instruments = items
                
                if (sSelf.instruments.count == 0) {
                    sSelf.informationViewShow(.error)
                }
                else {
                    sSelf.informationViewHide()
                    sSelf.tableView.reloadData()
                }
            }
        }
    }
    
    private func informationViewHide() {
        informationView.isHidden = true
    }
    
    private func informationViewShow(_ status: Status) {
        switch status {
        case .loading:
            informationTopConstraints.constant = 8
            informationLabel.text = "Loading instruments ..."
            informationIndicator.isHidden = false
            informationIndicator.startAnimating()
            break
        case .error:
            informationTopConstraints.constant = 8
            informationLabel.text = "Unable to load instruments."
            informationIndicator.isHidden = true
            informationIndicator.stopAnimating()
            break
        case .searching:
            informationTopConstraints.constant = 58
            informationLabel.text = "Searching instruments ..."
            informationIndicator.isHidden = false
            informationIndicator.startAnimating()
            break
        case .empty:
            informationTopConstraints.constant = 58
            informationLabel.text = "Sorry, nothing found."
            informationIndicator.isHidden = true
            informationIndicator.stopAnimating()
            break
        }
        informationView.isHidden = false
        view.bringSubview(toFront: informationView)
    }
    
    private func getInstrument(indexPath: IndexPath) -> EMInstrument {
        return instruments[indexPath.section].items[indexPath.row]
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return instruments[section].items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return instruments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EMInstrumentListViewController.cellId, for: indexPath) as! EMInstrumentCellView

        let instrument = instruments[indexPath.section].items[indexPath.row]
        cell.descriptionLabel.text = instrument.description
        cell.query = searchTextField.text ?? ""
        let checked = selectedSymbols.contains(instrument.symbol)
        cell.accessoryType = checked ? .checkmark : .none
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        var result = [String]()
        for item in instruments {
            if let title = item.group.groupName.characters.first {
                result.append(String(title))
            }
        }
        return result
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: EMInstrumentListViewController.sectionId) as! EMInstrumentSectionHeaderView
        let group = instruments[section]
        cell.headerLabel.text = group.group.groupName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let instrument = getInstrument(indexPath: indexPath)
            let checked = !selectedSymbols.contains(instrument.symbol)
            
            if (checked) {
                selectedSymbols.insert(instrument.symbol)
            } else {
                selectedSymbols.remove(instrument.symbol)
            }
            
            cell.accessoryType = checked ? .checkmark : .none
            
            addButton?.isEnabled = selectedSymbols.count > 0
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(searchTextFieldChanged(_:)), object: textField)
        perform(#selector(searchTextFieldChanged(_:)), with: textField, afterDelay: 0.3)
        return true
    }    
    
    func searchTextFieldChanged(_ textField: UITextField) {
        let query = textField.text ?? ""

        informationViewShow(.searching)
        
        storage.filterFor(text: query) { [weak self] (items:[EMInstruments]) in
            if let sSelf = self {
                sSelf.selectedSymbols.removeAll()
                sSelf.addButton?.isEnabled = false
                sSelf.instruments = items
                
                sSelf.tableView.reloadData()
                if (items.count == 0) {
                    sSelf.informationViewShow(.empty)
                } else {
                    sSelf.informationViewHide()
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
